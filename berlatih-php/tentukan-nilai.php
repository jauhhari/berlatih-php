<?php
function tentukan_nilai($number)
{
    if ($number <= 100) {
        if ($number >= 85) {
            return "Sanga Baik";
        } elseif ($number >= 70) {
            return "Baik";
        } elseif ($number >= 60) {
            return "Cukup";
        } else {
            return "Kurang";
        }
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>