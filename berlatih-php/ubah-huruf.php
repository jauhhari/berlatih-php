<?php
function ubah_huruf($string){
   $huruf = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't','u','v','w','x','y','z'];
   $hurufnew = "";

   for ($i = 0; $i < strlen($string); $i++) {
     for ($x=0; $x < count($huruf); $x++) {
       if ($huruf[$x] == $string[$i]) {
         $hurufnew .= $huruf[$x + 1];
       } 
     }
   }
   return $string ." => ". $hurufnew . '<br>';
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>