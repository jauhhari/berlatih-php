<?php

class Ape extends animal{
    public function __construct($name, $legs = 2, $cold_blooded = false)
    {
        $this->name         = $name;
        $this->legs         = $legs;
        $this->cold_blooded = $cold_blooded;
    }
    public function yell(){
        echo "Auooo";
    }
}