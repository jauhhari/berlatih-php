<?php 
    class animal{
        public $name;
        public $legs;
        public $cold_blooded;

        public function __construct($name, $legs = 2, $cold_blooded = false){
            $this->name         = $name;
            $this->legs         = $legs;
            $this->cold_blooded = $cold_blooded;
        }

        public function cetak()
        {
            echo $this->name;
            echo "<br>";
            echo $this->legs;
            echo "<br>";
            echo json_encode($this->cold_blooded);
            echo "<br>";
        }
    }
?>