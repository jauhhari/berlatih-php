<?php

require_once 'animal.php';
require("frog.php");
require("ape.php");


echo "<h3> Release 0 </h3>";

$sheep = new Animal("shaun");
$sheep->cetak();

echo "<h3> Release 1 </h3>";

$kodok = new Frog("buduk");
$kodok->cetak();
$kodok->jump(); // "hop hop"

$sungokong = new Ape("kera sakti");
$sungokong->cetak();
$sungokong->yell() // "Auooo"

?>